import java.awt.event.ActionEvent;
import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;


class ClientSomthing
{

    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private BufferedReader inputUser; // поток чтения с консоли
    private String addr; // ip адрес клиента
    private int port; // порт соединения
    private String nickname; // имя клиента
    private Date time;
    private String dtime;
    private SimpleDateFormat dt1;
    GraphicsModule g;


    public ClientSomthing(String addr, int port)
    {
        g = new GraphicsModule();
        g.window();
        this.addr = addr;
        this.port = port;
        try {
            this.socket = new Socket(addr, port);
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
        try {
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
            this.pressNickname(); // перед началом необходимо спросит имя
            new ReadMsg().start(); // нить читающая сообщения из сокета в бесконечном цикле
            new WriteMsg().start(); // нить пишущая сообщения в сокет приходящие с консоли в бесконечном цикле
        } catch (IOException e) {
            ClientSomthing.this.downService();
        }
    }

    private void pressNickname()
    {
        try {
            nickname = g.getName();
///            storageNme.add(nickname);
//            g.setList(storageNme);
            out.writeObject(nickname);
            out.flush();
        } catch (IOException ignored) {
        }

    }

    private void downService()
    {
        try {
            if (!socket.isClosed()) {
                g.delete(nickname);
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {
        }
    }

    private String str;

    private class ReadMsg extends Thread
    {
        @Override
        public void run()
        {
            while (true) {
                try {
                    Object o = in.readObject();
                    System.out.println("Got obj");
                    if (o instanceof LinkedList) {
                        g.setList((LinkedList) o);
                        System.out.println("this is list");
                    } else {
                        System.out.println("this is string");
                        str = (String) o;
                        if (str.equals("exit")) {
                            ClientSomthing.this.downService(); // харакири
                            break;
                        }
                        g.addText(str);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

        private String userWord;

        public class WriteMsg extends Thread
        {

            @Override
            public void run()
            {
                g.sendMessage.addActionListener((ActionEvent e) ->
                {
                    time = new Date();
                    dt1 = new SimpleDateFormat("HH:mm:ss"); // берем только время до секунд
                    dtime = dt1.format(time); // время
                    userWord = g.showText();
                    if (userWord.equals("exit")) {
                        try {
                            out.writeObject("exit" + "\n");
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        ClientSomthing.this.downService(); // харакири
                        //                           break; // выходим из цикла если пришло "stop"
                    } else {
                        try {
                            out.writeObject("(" + dtime + ") " + nickname + ": " + userWord + "\n");
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                    try {
                        out.flush(); // чистим
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                });
            }
        }

        //   }
    }
public class Client {

    public static String ipAddr = "localhost";
    public static int port = 8080;


    public static void main(String[] args) {
        new ClientSomthing(ipAddr, port);
    }
}