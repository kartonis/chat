import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;

public class GraphicsModule extends JFrame implements Serializable
{
    private JTextArea userInput,
        userList, messages;
    public JButton sendMessage;
    private String nick;
    private boolean fin;
    private JPanel panel = new JPanel();


    public GraphicsModule()
    {
        Container container = new Container();
        container.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        container.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        messages = new JTextArea();
        messages.setBackground(Color.white);
        messages.setEnabled(false);
        messages.setLineWrap(true);
        messages.setWrapStyleWord(true);
        JScrollPane sm = new JScrollPane (messages, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        userList = new JTextArea();
        userList.setBackground(Color.white);
        userList.setLineWrap(true);
        userList.setEnabled(false);
        userList.setWrapStyleWord(true);
        JScrollPane sl = new JScrollPane (userList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        userInput = new JTextArea();
        userInput.setBackground(Color.white);
        userInput.setLineWrap(true);
        userInput.setWrapStyleWord(true);
        JScrollPane si = new JScrollPane (userInput, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        sendMessage = new JButton("Send");

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 0;
        constraints.ipady     = 375;
        constraints.gridy   = 0  ;  // нулевая ячейка таблицы по вертикали


        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.5;
        constraints.gridx = 0;      // нулевая ячейка таблицы по горизонтали
        container.add(sm, constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 1;      // первая ячейка таблицы по горизонтали
        container.add(sl, constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.ipady     = 45;   // кнопка высокая
        constraints.weightx   = 0.0;
        constraints.gridwidth = 1;    // размер кнопки в две ячейки
        constraints.gridx     = 0;    // нулевая ячейка по горизонтали
        constraints.gridy     = 1;    // первая ячейка по вертикали
        container.add(si, constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.ipady     = 0;    // установить первоначальный размер кнопки
       // constraints.weighty   = 1.0;  // установить отступ
        // установить кнопку в конец окна
        constraints.anchor    = GridBagConstraints.PAGE_END;
        constraints.insets    = new Insets(0, 0, 0, 0);  // граница ячейки по Y
        constraints.gridwidth = 2;    // размер кнопки в 2 ячейки
        constraints.gridx     = 1;    // первая ячейка таблицы по горизонтали
        constraints.gridy     = 1;    // вторая ячейка по вертикали

        container.add(sendMessage, constraints);



        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(600, 500);

        add(container);
        setLocationRelativeTo(null);
        setTitle("Chat");
        panel.setBackground(Color.WHITE);
        setBackground(Color.white);
        setVisible(true);
    }

    public String showText()//выдать то, что написано
    {
        return userInput.getText();
    }


    public boolean flag(){
        fin = false;
        sendMessage.addActionListener((ActionEvent e) -> {
            fin = true;
        });
        return fin;
    }
    public void addText(String s)//добавить текст
    {
        messages.append(s);
    }

    public void window()
    {
                nick = JOptionPane.showInputDialog(
                        GraphicsModule.this,
                        "What is your nick?", nick);

                JOptionPane.showMessageDialog(GraphicsModule.this, "Hello "+ nick);
              //  userList.append(nick);


            }

    public void setList (LinkedList list){
        String s = "";


        for (int i = 0; i < list.size(); ++i) {
            s += (String) list.get(i) + "\n";
        }
        System.out.println("text is: " + s);
        userList.setText(s);
    }
    public void delete(String s){
        userList.setText( userList.getText().replaceAll(s, "" ) ) ;
    }
    public String getName(){
        return nick;
    }
}
