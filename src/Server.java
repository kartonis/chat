import java.io.*;
import java.net.*;
import java.util.LinkedList;

class ServerSomthing extends Thread {

    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Server server;


    public ServerSomthing(Socket socket, Server server, LinkedList<String> name) throws IOException {
        this.socket = socket;
        this.server = server;
        in = new ObjectInputStream(socket.getInputStream());
        out = new ObjectOutputStream(socket.getOutputStream());
        server.story.printStory(out);
        try {
            name.add((String)in.readObject());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        start();
    }
    @Override
    public void run() {
        String word;
            try {
                // первое сообщение отправленное сюда - это никнейм
//                word = (String) in.readObject();
                // out.writeObject(word + "\n");
//                    out.writeObject(name);
//                    out.flush(); // flush() нужен для выталкивания оставшихся данных
                // если такие есть, и очистки потока для дьнейших нужд
                while (true) {
                    word = (String) in.readObject();
                    if(word.equals("exit")) {
                        this.downService();
                        break;
                    }

                    server.broadcast(word + "\n");
                }
            } catch (NullPointerException ignored) {} catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
    }

    public  <T> void send(T msg) {
        try {
            out. writeObject(msg);
            out.flush();
        } catch (IOException ignored) {}
    }

    private void downService() {
        try {
            if(!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (ServerSomthing vr : server.serverList) {
                    if(vr.equals(this)) vr.interrupt();
                    server.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {}
    }
}


class Story {

    private LinkedList<String> story = new LinkedList<>();


    public void addStoryEl(String el) {
        if (story.size() >= 10) {
            story.removeFirst();
            story.add(el);
        } else {
            story.add(el);
        }
    }


    public void printStory(ObjectOutputStream writer) {
        if(story.size() > 0) {
            try {
                writer.writeObject("History messages" + "\n");
                for (String vr : story) {
                    writer.writeObject(vr + "\n");
                }
                writer.writeObject("/...." + "\n");
                writer.flush();
            } catch (IOException ignored) {}

        }

    }
}

public class Server {

    public static final int PORT = 8080;
    public LinkedList<ServerSomthing> serverList;
    public Story story;
    public LinkedList<String > name;
    public ServerSocket server;

    Server() throws IOException
    {
        server = new ServerSocket(PORT);
        story = new Story();
        name = new LinkedList<>();
        serverList = new LinkedList<>();
    }

    void run() throws IOException
    {
        System.out.println("Server Started");
        try {
            while (true) {
                Socket socket = null;
                try {
                    socket = server.accept();
                    serverList.add(new ServerSomthing(socket, this, name));

                    LinkedList<String> dupnames = new LinkedList<>(name);
                    for (ServerSomthing client : serverList)
                    {
                        client.send(dupnames);
                    }

                } catch (IOException e) {
                    if (socket != null) socket.close();
                }
            }
        } finally {
            server.close();
        }
    }

    void broadcast(String word)
    {
        story.addStoryEl(word);
        for (ServerSomthing vr : serverList) {
            vr.send(word);
        }
    }

    public static void main(String[] args)
    {

        Server server = null;
        try {
            server = new Server();
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}