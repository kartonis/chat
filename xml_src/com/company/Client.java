package com.company;

import java.awt.event.ActionEvent;
import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;


class ClientSomthing
{
    private Socket socket;
    //private ObjectOutputStream out;
    // private ObjectInputStream in;
    private BufferedReader inputUser; // поток чтения с консоли
    private String addr; // ip адрес клиента
    private int port; // порт соединения
    private String nickname; // имя клиента
    private Date time;
    private String dtime;
    private SimpleDateFormat dt1;
    GraphicsModule g;
    WriteXml writeXml;
    ReadXML readXML;
    BufferedInputStream bis;
    BufferedOutputStream bos;
    byte[] byteArray;
    File file;


    public ClientSomthing(String addr, int port)
    {
        file = new File("Checkk.xml");

        readXML = new ReadXML();
        writeXml = new WriteXml();
        g = new GraphicsModule();
        g.window();
        this.addr = addr;
        this.port = port;
        try {
            this.socket = new Socket(addr, port);
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
        inputUser = new BufferedReader(new InputStreamReader(System.in));
        this.pressNickname(); // перед началом необходимо спросит имя
        new ReadMsg().start(); // нить читающая сообщения из сокета в бесконечном цикле
        new WriteMsg().start(); // нить пишущая сообщения в сокет приходящие с консоли в бесконечном цикле
    }

    public void sendXml()
    {/////передача длины файла и файла
        try {
            File f = writeXml.getFile();

            ///get a len of file

            long len = f.length();

            DataOutputStream out = null;
            try {
                //create write stream to send information
                out = new DataOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                //Bail out
            }

            out.writeInt((int) len);

            int in = 0;

            bis = new BufferedInputStream(new FileInputStream(f));
            try {
                bos = new BufferedOutputStream(socket.getOutputStream());
                byteArray = new byte[(int) len];
                in = bis.read(byteArray, 0, (int) len);//) != -1) {
                bos.write(byteArray, 0, in);
                bos.flush();
                //}
            } catch (IOException e) {
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressNickname()
    {
        nickname = g.getName();
        ///print to xml
        writeXml.write(nickname, "login", "name");
        sendXml();
    }

    private void downService()
    {
        try {
            if (!socket.isClosed()) {
                g.delete(nickname);
                socket.close();
                bis.close();
                bos.close();
            }
        } catch (IOException ignored) {
        }
    }

    private void getXml()
    {
        int lenFile = 0;
        DataInputStream in = null;
        try {
            int num = 0;
            in = new DataInputStream(socket.getInputStream());
            lenFile = in.readInt();
            System.out.println("recieved len " + lenFile);
            try {
                bis = new BufferedInputStream(socket.getInputStream());
                byteArray = new byte[lenFile];


                num = bis.read(byteArray, 0, lenFile);

                System.out.println("recieved num bytes " + num);

                try (FileOutputStream fos = new FileOutputStream(file)) {
                    fos.write(byteArray);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String str;

    private class ReadMsg extends Thread
    {
        @Override
        public void run()
        {
            ParserXml parserXml = new ParserXml();
            while (true) {
                getXml();
                System.out.println("recieved");

                if (parserXml.parse(file).equals("list")) {
                    str = readXML.getInfo(file, "session");
                    g.setList(str);
                } else {
                    str = readXML.getInfo(file, "message");
                    if (str.equals("exit")) {
                        ClientSomthing.this.downService(); // харакири
                        break;
                    }
                    g.addText(str);
                }
            }
        }
    }

    private String userWord;

    public class WriteMsg extends Thread
    {

        @Override
        public void run()
        {
            g.sendMessage.addActionListener((ActionEvent e) ->
            {
                time = new Date();
                dt1 = new SimpleDateFormat("HH:mm:ss"); // берем только время до секунд
                dtime = dt1.format(time); // время
                userWord = g.showText();
                if (userWord.equals("exit")) {
                    writeXml.write("exit" + "\n", "logout", "session");
                    sendXml();
                    ClientSomthing.this.downService();
                } else {

                    writeXml.write("(" + dtime + ") " + nickname + ": " + userWord + "\n", "message", "message");
                    sendXml();
                }
            });
        }

    }
}
    public class Client
    {
        public static String ipAddr = "localhost";
        public static int port = 8080;

        public static void main(String[] args)
        {
            new ClientSomthing(ipAddr, port);
        }
    }
