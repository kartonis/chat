package com.company;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;

public class ParserXml
{
    public String parse(File file){

        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            org.w3c.dom.Document doc = db.parse(file);
            NodeList base = doc.getElementsByTagName("command");
            Node basenode = base.item(0);

            return basenode.getAttributes().item(0).getNodeValue();
        }
        catch (Exception e) {
            e.printStackTrace(); return null;}
//        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder documentBuilder = null;
//        try {
//            documentBuilder = documentFactory.newDocumentBuilder();
//        } catch (ParserConfigurationException e) {
//            e.printStackTrace();
//        }
//        Document document = documentBuilder.newDocument();
//        NamedNodeMap attr = document.getAttributes();
//        System.out.println(attr.item(0).getNodeValue());
//        return attr.item(0).getNodeValue();

    }
}
