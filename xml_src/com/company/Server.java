package com.company;

import java.io.*;
import java.net.*;
import java.util.LinkedList;

import static java.lang.System.in;
import static java.lang.System.out;

class ServerSomthing extends Thread
{

    private Socket socket;
    private Server server;
    private BufferedInputStream bis;
    private BufferedOutputStream bos;
    private byte[] byteArray;
    ReadXML readXML;
    WriteXml writeXml;
    File file;


    public ServerSomthing(Socket socket, Server server, LinkedList<String> name, WriteXml writeXml) throws IOException
    {
        this.socket = socket;
        readXML = new ReadXML();
        this.writeXml = writeXml;
        this.server = server;
        file = new File("Checkk.xml");
        getXml();
        name.add(readXML.getInfo(file, "name"));
        server.story.printStory(writeXml);

        if (server.story.size() > 0)
            sendXml();

        start();
    }

    public void sendXml()
    {/////передача длины файла и файла
        try {
            File f = writeXml.getFile();

            ///get a len of file

            long len = f.length();

            DataOutputStream out = null;
            try {
                //create write stream to send information
                out = new DataOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                //Bail out
            }

            out.writeInt((int) len);

            int in = 0;

            bis = new BufferedInputStream(new FileInputStream(f));
            try {
                bos = new BufferedOutputStream(socket.getOutputStream());
                byteArray = new byte[(int) len];
                in = bis.read(byteArray, 0, (int) len);//) != -1) {
                bos.write(byteArray, 0, in);
                bos.flush();
                //}
            } catch (IOException e) {
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getXml()
    {
        int lenFile = 0;
        DataInputStream in = null;
        try {
            int num = 0;
            in = new DataInputStream(socket.getInputStream());
            lenFile = in.readInt();

            try {
                bis = new BufferedInputStream(socket.getInputStream());
                byteArray = new byte[lenFile];


                num = bis.read(byteArray, 0, lenFile);


                try (FileOutputStream fos = new FileOutputStream(file)) {
                    fos.write(byteArray);
                }
            } catch (IOException e) {
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run()
    {
        String word;
        try {
            while (true) {
                getXml();
                word = readXML.getInfo(file, "message");
                if (word.equals("exit")) {
                    this.downService();
                    break;
                }

                server.broadcast(word + "\n");
            }
        }
        catch (Exception e) {
        }
    }

    public void send(String msg, boolean list) {
        if (list){
            writeXml.write(msg, "list", "session");
        }
        else
            writeXml.write(msg, "message", "message");

        sendXml();
    }

    private void downService() {
        try {
            if(!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (ServerSomthing vr : server.serverList) {
                    if(vr.equals(this)) vr.interrupt();
                    server.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {}
    }
}


class Story {

    private LinkedList<String> story = new LinkedList<>();

    public int size() { return story.size(); }

    public void addStoryEl(String el) {
        if (story.size() >= 10) {
            story.removeFirst();
            story.add(el);
        } else {
            story.add(el);
        }
    }


    public void printStory(WriteXml writeXml) {
        if(story.size() > 0) {
            String s = "";
            for (String vr : story) {
                s += vr + "\n";
            }
            writeXml.write(s, "message", "message");

        }

    }
}

public class Server {

    public static final int PORT = 8080;
    public LinkedList<ServerSomthing> serverList;
    public Story story;
    public LinkedList<String > name;
    public ServerSocket server;
    public WriteXml writeXml;


    Server() throws IOException
    {
        writeXml = new WriteXml();
        server = new ServerSocket(PORT);
        story = new Story();
        name = new LinkedList<>();
        serverList = new LinkedList<>();
    }

    void run() throws IOException
    {
        out.println("Server Started");
        try {

            while (true) {
                Socket socket = null;
                try {
                    socket = server.accept();
                    serverList.add(new ServerSomthing(socket, this, name, writeXml));

                    String string = "";
                    for (String s: name) {
                        string+=s+"\n";
                    }
                    System.out.println("sending list");
                    for (ServerSomthing client : serverList)
                    {
                        client.send(string, true);
                    }

                } catch (IOException e) {
                    if (socket != null) socket.close();
                }
            }
        } finally {
            server.close();
        }
    }

    void broadcast(String word)
    {
        story.addStoryEl(word);
        for (ServerSomthing vr : serverList) {
            vr.send(word, false);
        }
    }

    public static void main(String[] args)
    {
        Server server = null;
        try {
            server = new Server();
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}